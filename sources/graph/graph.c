/*
** graphe.c for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/sources
**
** Made by lapray_o
** Login   <lapray_o@epitech.net>
**
** Started on  Tue Apr 15 16:29:20 2014 lapray_o
** Last update Wed Apr 30 17:00:52 2014 Gregoire Ballot
*/

#include <stdlib.h>
#include "../../includes/lemin.h"
#include "lib.h"

int		add_room(t_room **room, char *name, t_room_data *data)
{
  static int	id = 0;
  t_room	*new_room;

  if ((new_room = malloc(sizeof(t_room))) == NULL)
    return (FAILURE);
  if ((new_room->room_name = my_strdup(name)) == NULL)
    return (FAILURE);
  new_room->ant = 0;
  new_room->id = ++id;
  new_room->x = data->x;
  new_room->y = data->y;
  new_room->nbr_links = 0;
  new_room->status = data->status;
  new_room->next = (*room);
  new_room->links = NULL;
  (*room) = new_room;
  return (SUCCESS);
}

t_room		*get_room_from_name(t_room *room, char *name)
{
  while (room != NULL)
    {
      if (my_strcmp(room->room_name, name) == 0)
	return (room);
      room = room->next;
    }
  return (NULL);
}

static int	create_link(t_room *room1, t_room *room2)
{
  int		*newtab;
  int		i;

  i = -1;
  if (room1->links != NULL)
    while (++i < room1->nbr_links)
      if (room1->links[i] == room2->id)
	return (SUCCESS);
  i = -1;
  ++(room1->nbr_links);
  if ((newtab = malloc(sizeof(int *) * room1->nbr_links)) == NULL)
    return (FAILURE);
  if (room1->links != NULL)
    {
      while (++i < room1->nbr_links)
	if (i == (room1->nbr_links - 2))
	  newtab[i] = room2->id;
	else
	  newtab[i] = room1->links[i];
      free(room1->links);
    }
  else
    newtab[0] = room2->id;
  room1->links = newtab;
  return (SUCCESS);
}

int		link_rooms(t_room *room, char *name1, char *name2)
{
  t_room	*temp1;
  t_room	*temp2;

  if (my_strcmp(name1, name2) == 0)
    {
      my_putstr("Warning : Link between two rooms\n");
      return (SUCCESS);
    }
  if (((temp1 = get_room_from_name(room, name1)) == NULL)
      || ((temp2 = get_room_from_name(room, name2)) == NULL))
    return (FAILURE);
  if (create_link(temp1, temp2) == FAILURE)
    return (FAILURE);
  if (create_link(temp2, temp1) == FAILURE)
    return (FAILURE);
  return (SUCCESS);
}
