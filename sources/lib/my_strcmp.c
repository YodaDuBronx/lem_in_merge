/*
** my_strcmp.c for  in /home/ballot_g/rendu/VM
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Sun Apr 13 11:01:33 2014 Gregoire Ballot
** Last update Mon Apr 28 21:32:16 2014 lapray_o
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] && s2[i])
    {
      if (s1[i] != s2[i])
	return (-1);
      else
	++i;
    }
  return (0);
}
