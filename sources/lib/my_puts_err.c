/*
** my_puts_err.c for lem_in in /home/lapray_o/rendu/prog_elem/common_rep/lem_in_merge/sources/lib
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Sat Apr 26 17:49:31 2014 lapray_o
** Last update Sat Apr 26 18:00:39 2014 lapray_o
*/

#include <unistd.h>

void	my_putc_err(char c)
{
  write(2, &c, 1);
}

void	my_puts_err(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putc_err(str[i]);
      i++;
    }
}
