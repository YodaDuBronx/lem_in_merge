/*
** my_putstr.c for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/lib
**
** Made by lapray_o
** Login   <lapray_o@epitech.net>
**
** Started on  Mon Apr 14 12:54:35 2014 lapray_o
** Last update Sat Apr 26 18:00:06 2014 lapray_o
*/

#include "lib.h"

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
}
