/*
** my_str_to_wordtab.c for  in /home/ballot_g/rendu/Piscine-C-Jour_08/ex_03
**
** Made by ballot_g
** Login   <ballot_g@epitech.net>
**
** Started on  Sun Oct 20 15:36:12 2013 ballot_g
** Last update Sat Apr 26 15:45:56 2014 Gregoire Ballot
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static int	countwords(char *str)
{
  int		i;
  int		cpt;

  cpt = 0;
  i = 0;
  while (str[i] != '\0')
    {
      if (((str[i] == '\n') && (str[i - 1] != '\n')) ||
	  ((str[i] != '\n') && (str[i + 1] == '\0')))
	cpt = cpt + 1;
      i = i + 1;
    }
  return (cpt);
}

static char	*getword(char *str, int pos)
{
  char		*s;
  int		i;
  int		c;

  i = pos;
  c = 0;
  while (str[i] && str[i] != '\n')
    {
      i = i + 1;
      c = c + 1;
    }
  if ((s = malloc(c + 1)) == NULL)
    return (NULL);
  i = 0;
  while (i < c)
    {
      s[i] = str[pos];
      i = i + 1;
      pos = pos + 1;
    }
  s[i] = '\0';
  return (s);
}

char		**my_str_to_wordtab(char *str)
{
  char		**wordtab;
  int		i;
  int		j;

  i = 0;
  j = 0;
  if (*str == 0)
    return (NULL);
  if ((wordtab = malloc((countwords(str) + 1) * sizeof(char *))) == NULL)
    return (NULL);
  wordtab[j++] = getword(str, 0);
  while (j < (countwords(str)) && str[i])
    {
      if (str[i] == '\n')
	wordtab[j++] = getword(str, i + 1);
      i = i + 1;
    }
  wordtab[j] = NULL;
  return (wordtab);
}
