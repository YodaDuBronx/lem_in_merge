/*
** my_getnbr.c for lem_in in /home/lapray_o/rendu/prog_elem/real_lemin/lem_in_merge
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Mon Apr 28 19:05:28 2014 lapray_o
** Last update Mon Apr 28 19:05:29 2014 lapray_o
*/

int	my_getnbr(char *str)
{
  int	i;
  int	multi;
  int	result;
  int	minus;

  i = 0;
  multi = 1;
  result = 0;
  minus = 0;
  while (str[i] != 0)
    {
      if (str[i] == '-')
        minus = minus + 1;
      i = i + 1;
    }
  i = i - 1;
  while (i >= 0 && str[i] <= 57 && str[i] >= 48)
    {
      result = result + (str[i] - '0') * multi;
      multi = multi * 10;
      i = i - 1;
    }
  if (minus % 2 != 0 && minus != 0)
    result = result * -1;
  return (result);
}
