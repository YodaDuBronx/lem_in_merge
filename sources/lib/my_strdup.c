/*
** my_strdup.c for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/lib
**
** Made by lapray_o
** Login   <lapray_o@epitech.net>
**
** Started on  Mon Apr 21 17:09:00 2014 lapray_o
** Last update Sat Apr 26 17:59:48 2014 lapray_o
*/

#include <stdlib.h>
#include "lib.h"

char	*my_strdup(char *str)
{
  int	i;
  char	*str2;

  i = 0;
  if ((str2 = malloc(my_strlen(str) + 1)) == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      str2[i] = str[i];
      i++;
    }
  str2[i] = '\0';
  return (str2);
}
