/*
** my_strlen.c for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/lib
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Mon Apr 14 12:55:43 2014 lapray_o
** Last update Mon Apr 14 12:56:22 2014 lapray_o
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    i = i + 1;
  return (i);
}
