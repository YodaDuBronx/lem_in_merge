/*
** my_putchar.c for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/lib
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Mon Apr 14 12:54:02 2014 lapray_o
** Last update Mon Apr 14 13:15:15 2014 lapray_o
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
