/*
** check_breakpoint.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.eu>
**
** Started on  Mon Apr 28 15:17:37 2014 Gregoire Ballot
** Last update Tue Apr 29 23:00:35 2014 lapray_o
*/

#include <stdlib.h>
#include "lib.h"
#include "lemin.h"

static char	*get_room_name(char *line, int *i, int *j)
{
  char		*name;

  if ((name = malloc(my_strlen(line))) == NULL)
    return (NULL);
  while (line[*i] && line[*i] != ' ')
    {
      name[*i] = line[*i];
      ++(*i);
    }
  name[*i] = 0;
  *j = *i;
  if (line[*i] == 0)
    return (NULL);
  return (name);
}

static void	init_vars(int *i, int *j, t_room_data *data, int status)
{
  *i = 0;
  *j = 0;
  data->x = 0;
  data->x = 0;
  data->status = status;
}

int		parse_room(t_room **graph, char *line, int status)
{
  char		*name;
  t_room_data	data;
  int		i;
  int		j;

  init_vars(&i, &j, &data, status);
  if ((name = get_room_name(line, &i, &j)) == NULL)
    return (-1);
  while (line[i] && line[i] != ' ')
    ++i;
  if (line[i] == 0)
    return (-1);
  data.x = my_get_focused_nbr(line, j, i);
  j = i + 1;
  while (line[i] && line[i] != ' ')
    ++i;
  data.y = my_get_focused_nbr(line, j, i);
  if (add_room(graph, name, &data) == -1)
    return (-1);
  free(name);
  return (0);
}

int		check_break_point(char *command, char *next_line,
				  t_room **graph, int *i)
{
  if (my_strlen(command) > 2)
    {
      if (my_strcmp(&command[1], "start") == -1)
	{
	  *i = *i + 1;
	  if ((parse_room(graph, next_line, START_ROOM)) == -1)
	    return (-1);
	}
      else if (my_strcmp(&command[1], "end"))
	{
	  *i = *i + 1;
	  if ((parse_room(graph, next_line, END_ROOM)) == -1)
	    return (-1);
	}
    }
  return (0);
}
