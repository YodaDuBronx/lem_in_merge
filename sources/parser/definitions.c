/*
** definitions.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Mon Apr 28 09:46:24 2014 Gregoire Ballot
** Last update Mon Apr 28 15:23:54 2014 Gregoire Ballot
*/

#include <stdlib.h>
#include "../../includes/lemin.h"
#include "../../includes/lib.h"

static int	def_are_over(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '-')
	return (0);
      ++i;
    }
  return (1);
}

int		my_get_focused_nbr(char *str, int beg, int end)
{
  int		nbr;

  nbr = 0;
  while (beg < end)
    nbr = (nbr * 10) - (str[beg++] - '0');
  return (nbr * (-1));
}

int		get_definitions(char **file, t_graphinf *info,
				t_room **graph, int i)
{
  while (file[i])
    {
      if (def_are_over(file[i]) == 0)
	return (i);
      if (file[i][0] == '#' && (my_strlen(file[i]) > 1))
	{
	  if (file[i][1] == '#')
	    check_break_point(file[i], file[i + 1], graph, &i);
 	}
      else
	if ((parse_room(graph, file[i], BASIC_ROOM)) == -1)
	  {
	    my_puts_err("Invalid line\n");
	    return (-1);
	  }
      ++i;
    }
  return (i);
}
