/*
** parser.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.eu>
**
** Started on  Mon Apr 28 15:17:30 2014 Gregoire Ballot
** Last update Wed Apr 30 17:04:10 2014 Gregoire Ballot
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "wordtab.h"
#include "lemin.h"
#include "lib.h"

static char	*my_buffer_cat(char *buffer, char *file_content, int size)
{
  char		*newbuffer;
  int		i;
  int		j;

  j = 0;
  i = 0;
  if ((newbuffer = malloc(size + 1)) == NULL)
    return (NULL);
  while (file_content[i])
    {
      newbuffer[i] = file_content[i];
      ++i;
    }
  while (buffer[j])
    {
      newbuffer[i] = buffer[j];
      ++i;
      ++j;
    }
  newbuffer[i] = 0;
  free(file_content);
  return (newbuffer);
}

static int	valid_buffer(char *str)
{
  int		dash;
  int		i;

  i = 0;
  dash = 0;
  if (*str == 0 || *str == 10)
    return (-1);
  while (str[i])
    {
      if (str[i] == '-')
	++dash;
      if (dash > 1)
	return (-1);
      if (str[i] == '\n')
	dash = 0;
      ++i;
    }
  return (0);
}

static char	*init_values(char *file, int *a, int *b)
{
  a = 0;
  b = 0;
  if (((file = malloc(READSIZE + 1)) == NULL))
    return (NULL);
  *file = 0;
  return (file);
}

static char	**get_filedata()
{
  char		buffer[READSIZE + 1];
  char		*file_content;
  int		size;
  int		ret;
  int		check;
  char		**file_lines;

  file_content = NULL;
  if ((file_content = init_values(file_content, &size, &check)) == NULL)
    return (NULL);
  while ((ret = read(0, buffer, READSIZE)) > 0)
    {
      size += ret;
      buffer[ret] = 0;
      if ((file_content = my_buffer_cat(buffer, file_content, size)) == NULL)
	return (NULL);
      if (valid_buffer(file_content) == -1)
	return (NULL);
    }
  if (ret == -1)
    return (NULL);
  file_lines = my_str_to_wordtab(file_content);
  free(file_content);
  return (file_lines);
}

t_room		*parser()
{
  t_graphinf	info;
  t_room	*graph;
  char		**file;

  info.density = 0;
  info.size = 0;
  my_putstr("Parsing entry\n");
  if ((file = get_filedata()) == NULL)
    {
      my_puts_err("Could not get data\n");
      return (NULL);
    }
  if ((graph = fill_graph(file, &info)) == NULL)
    {
      my_free_wordtab(file);
      my_puts_err("Could not parse data\n");
      return (NULL);
    }
  my_free_wordtab(file);
  return (graph);
}
