/*
** file_top.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.eu>
**
** Started on  Mon Apr 28 15:17:34 2014 Gregoire Ballot
** Last update Wed Apr 30 00:39:21 2014 lapray_o
*/

#include <stdio.h>
#include <stdlib.h>
#include "lemin.h"
#include "lib.h"

static int	my_isnum(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= '0' && str[i] <= '9')
	++i;
      else
	{
	  my_putstr("Invalid number of ants\n");
	  return (-1);
	}
    }
  return (0);
}

static int	get_ant_nb(char **file, t_graphinf *info)
{
  int		i;
  int		j;

  i = 0;
  while (file[i])
    {
      j = 0;
      if (file[i][j] == '#')
	++i;
      else
	{
	  if (my_isnum(file[i]) == -1)
	    return (-1);
	  else
	    info->density = my_getnbr(file[i]);
	  return (i + 1);
	}
    }
  return (-1);
}

t_room		*fill_graph(char **file, t_graphinf *info)
{
  t_room	*graph;
  int		i;

  i = 0;
  graph = NULL;
  if ((i = get_ant_nb(file, info)) == -1)
    return (NULL);
  if ((i = get_definitions(file, info, &graph, i)) == -1)
    return (NULL);
  my_putstr("File reading is over\n");
  if ((i = get_links(file, info, graph, i)) == -1)
    return (NULL);
  return (graph);
}
