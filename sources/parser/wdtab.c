/*
** wdtab.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.eu>
**
** Started on  Mon Apr 28 15:17:23 2014 Gregoire Ballot
** Last update Mon Apr 28 15:17:25 2014 Gregoire Ballot
*/

#include <stdlib.h>

int	my_wordtab_len(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    ++i;
  return (i);
}

void	my_free_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    {
      free(tab[i]);
      ++i;
    }
  free(tab);
}
