/*
** links.c for  in /home/ballot_g/rendu/lem_in/sources/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Mon Apr 28 11:49:06 2014 Gregoire Ballot
** Last update Wed Apr 30 16:24:51 2014 Gregoire Ballot
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "wordtab.h"
#include "lemin.h"
#include "lib.h"

static int	parse_link(char *line, t_room *graph)
{
  char		*rooms[2];
  int		i;
  int		j;

  i = 0;
  j = 0;
  if (((rooms[0] = malloc(my_strlen(line))) == NULL) ||
      ((rooms[1] = malloc(my_strlen(line))) == NULL))
    return (FAILURE);
  while (line[i] && (line[i] != '-'))
    rooms[0][i] = line[i++];
  rooms[0][i] = 0;
  if (line[i++] == 0)
    return (FAILURE);
  while (line[i])
    rooms[1][j++] = line[i++];
  rooms[1][j] = 0;
  if (my_strlen(rooms[1]) > 0)
    return (link_rooms(graph, rooms[0], rooms[1]));
  else
    return (FAILURE);
}

int		get_links(char **file, t_graphinf *info,
			  t_room *graph, int i)
{
  while (file[i])
    {
      if (file[i][0] != '#')
	{
	  if (parse_link(file[i], graph) == FAILURE)
	    return (FAILURE);
	}
      ++i;
    }
  return (i);
}
