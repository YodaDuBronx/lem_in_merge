/*
** a_star.c for lem_in in /home/lapray_o/rendu/prog_elem/real_lemin/lem_in_merge/algo
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Tue Apr 29 16:13:52 2014 lapray_o
** Last update Wed Apr 30 17:59:20 2014 lapray_o
*/

#include <stdlib.h>
#include "algo.h"
#include "lib.h"

/* t_opened	*init_opened_list() */
/* { */
/*   t_opened	*new_opened; */

/*   if ((new_opened = malloc(sizeof(t_opened))) == NULL) */
/*     return (NULL); */
/*   (*new_opened).name = NULL; */
/*   (*new_opened).next = NULL; */
/*   return (new_opened); */
/* } */

/* t_choosen	*init_choosen_list() */
/* { */
/*   t_choosen	*new_choosen; */

/*   if ((new_choosen = malloc(sizeof(t_choosen))) == NULL) */
/*     return (NULL); */
/*   (*new_choosen).name = NULL; */
/*   (*new_choosen).next = NULL; */
/*   return (new_choosen); */
/* } */

t_list		init_list()
{
  t_list	*new_list;

  if ((new_list = malloc(sizeof(t_list))) == NULL)
    return (NULL);
  (*new_list).room = NULL;
  (*new_list).next = NULL;
  (*new_list).prev = NULL;
  return (new_list);
}

int		add_to_list(t_list **list, t_room **room) // on ajoute quoi à quoi ?
{
  t_list	*new_list;

  if ((new_list = malloc(sizeof(t_list))) == NULL)
    return (FAIL);
  while (((*list) != NULL) && ((*list)->next != NULL))
    (*list) = (*list)->next;
  (*new_list)->ptr = ; // = ?
  (*list) = new_list;
  (*list)->next = NULL;
  return (OK);
}

/* int		add_opened_elem(t_opened **opened, char *name) */
/* { */
/*   t_opened	*new_opened; */

/*   if ((new_opened = malloc(sizeof(t_opened))) == NULL) */
/*     return (FAIL); */
/*   if (((*new_opened).name = my_strdup(name)) == NULL) */
/*     return (FAIL); */
/*   while ((*opened)->name != NULL) */
/*     (*opened) = (*opened)->next; */
/*   (*opened) = new_opened; */
/*   (*opened)->next = NULL; */
/*   return (OK); */
/* } */

/* int		add_choosen_elem(t_choosen **choosen, char *name) */
/* { */
/*   t_choosen	*new_choosen; */

/*   if ((new_choosen = malloc(sizeof(t_choosen))) == NULL) */
/*     return (FAIL); */
/*   if (((*new_choosen).name = my_strdup(name)) == NULL) */
/*     return (FAIL); */
/*   while ((*choosen)->name != NULL) */
/*     (*choosen) = (*choosen)->next; */
/*   (*choosen) = new_choosen; */
/*   (*choosen)->next = NULL; */
/*   return (OK); */
/* } */
