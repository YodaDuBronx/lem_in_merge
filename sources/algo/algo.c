/*
** algo.c for lem_in in /home/lapray_o/rendu/prog_elem/real_lemin/lem_in_merge/algo
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Tue Apr 29 17:06:06 2014 lapray_o
** Last update Wed Apr 30 18:00:22 2014 lapray_o
*/

#include <stdlib.h>
#include "algo.h"

  /*
  ** ajouter begin à close_list;
  ** 	---
  ** ajouter dans open_list les nodes linkés au [dernier element ajouté dans close_list];
  ** trouver le node de open_list qui a la "credibilité" la plus haute (--> le plus de liens);
  **   -> si ta fonction find_most_adj renvoie -1, [2 cas:]  -> soit t'es revenu sur begin alors print "pas de solution"
  **							      -> soit tu vires le dernier element ajouté a close_list de close_list
  ** close_flag de ce node = 1 && le foutre dans close_list && vidation de open_list;
  **    ---
  ** conditions d'arret: - si c'est ##end
  **			 - voir le truc sur find_most_adj
  */

int	find_most_adj(int *tab)
{
  int	i;
  int	save;
  int	save_i;

  i = 0;
  save = 0;
  save_i = -1;
  while (i <= (nbr_links - 1))
    {
      if (tab[i] > save)
	{
	  save_i = i;
	  save = tab[i];
	}
      i++;
    }
  return (save_i);
}

int		algo(t_room *room)
{
  t_list	*open;
  t_list	*close;

  if ((open = init_list()) == NULL)
    return (FAIL);
  if ((close = init_list()) == NULL)
    return (FAIL);
  return (SUCCESS);
}

