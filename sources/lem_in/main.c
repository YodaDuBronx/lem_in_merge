/*
** main.c for  in
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Sat Apr 26 13:04:11 2014 Gregoire Ballot
** Last update Wed Apr 30 00:20:15 2014 lapray_o
*/

#include <stdlib.h>
#include <unistd.h>
#include "lemin.h"
#include "wordtab.h"
#include "lib.h"

void		free_graph(t_room *graph)
{
  t_room	*tmp;

  while (graph != NULL)
    {
      tmp = graph;
      graph = graph->next;
      if (tmp->room_name)
	free(tmp->room_name);
      free(graph);
    }
}

int		main(int argc, char **argv)
{
  t_room	*graph;

  graph = NULL;
  if ((graph = parser()) == NULL)
    {
      my_puts_err("Entry Corrupted\n");
      return (-1);
    }
  free_graph(graph);
  return (0);
}
