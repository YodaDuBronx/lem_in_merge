##
## Makefile for  in /home/ballot_g/rendu/lem_in_merge
## 
## Made by Gregoire Ballot
## Login   <ballot_g@epitech.net>
## 
## Started on  Sat Apr 26 12:55:11 2014 Gregoire Ballot
## Last update Tue Apr 29 22:08:55 2014 lapray_o
##

CC = gcc 

SRCS =	./sources/lib/my_putchar.c		\
	./sources/lib/my_strdup.c		\
	./sources/lib/my_str_to_wordtab.c	\
	./sources/lib/my_strcmp.c		\
	./sources/lib/my_strlen.c		\
	./sources/lib/my_putstr.c		\
	./sources/lib/my_getnbr.c		\
	./sources/lib/my_puts_err.c		\
	./sources/parser/parser.c		\
	./sources/parser/wdtab.c		\
	./sources/parser/check_breakpoint.c	\
	./sources/parser/definitions.c		\
	./sources/parser/links.c		\
	./sources/parser/file_top.c		\
	./sources/graph/graph.c			\
	./sources/lem_in/main.c

OBJS =	$(SRCS:.c=.o)

NAME =	lem_in

CFLAGS += -g -I./includes/

all: $(NAME)

$(NAME): $(OBJS)
	gcc $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
