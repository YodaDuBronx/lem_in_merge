/*
** lib.h for lem_in in /home/lapray_o/rendu/prog_elem/lem_in/headers
**
** Made by lapray_o
** Login   <lapray_o@epitech.net>
**
** Started on  Mon Apr 14 13:15:56 2014 lapray_o
** Last update Wed Apr 30 15:33:37 2014 Gregoire Ballot
*/

#ifndef LIB_H_
# define LIB_H_

# define FAILURE	-1
# define SUCCESS	0

void	my_putchar(char);
void	my_putstr(char *);
void	my_puts_err(char *);
int	my_strlen(char *);
char	*my_strdup(char *);
int	my_strcmp(char *, char *);
int	my_getnbr(char *);

#endif /* !LIB_H_ */
