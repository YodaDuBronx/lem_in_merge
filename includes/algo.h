/*
** algo.h for lem_in in /home/lapray_o/rendu/prog_elem/real_lemin/lem_in_merge/algo
** 
** Made by lapray_o
** Login   <lapray_o@epitech.net>
** 
** Started on  Tue Apr 29 16:14:49 2014 lapray_o
** Last update Wed Apr 30 17:52:13 2014 lapray_o
*/

#ifndef ALGO_H_
# define ALGO_H_

# include "lemin.h"

# define FAIL -1
# define OK 0

typedef struct		s_opened
{
  char			*name;
  struct s_opened	*next;
}			t_opened;

typedef struct		s_list
{
  t_room		*ptr;
  struct s_list		*next;
  struct s_list		*prev;
}			t_list;

int		algo_begin();
t_opened	*init_opened_list();
t_choosen	*init_choosen_list();
int		add_opened_elem(t_opened**, char*);
int		add_choosen_elem(t_choosen**, char*);

#endif /* !ALGO_H_ */
