/*
** parser.h for lemin in /home/ballot_g/rendu/lem_in_merge/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Mon Apr 21 15:14:15 2014 Gregoire Ballot
** Last update Wed Apr 30 15:16:55 2014 Gregoire Ballot
*/

#ifndef LEMIN_H_
# define LEMIN_H_

/*
** Parsing defines
*/

# define READSIZE	512
# define START_ROOM	1
# define END_ROOM	2
# define BASIC_ROOM	3

# define YES		1
# define NO		0

/*
** Data structures containing main information
*/

typedef struct		s_room_data
{
  int			nb;
  char			status;
  int			x;
  int			y;
}			t_room_data;

typedef struct		s_room
{
  int			id;
  int			x;
  int			y;
  char			ant;
  char			status;
  char			*room_name;
  int			nbr_links;
  int			*links;
  struct s_room		*next;
}			t_room;

typedef struct		s_graphinf
{
  int			density;
  int			size;
}			t_graphinf;

/*
** Parser functions
*/

t_room			*parser();
t_room			*fill_graph(char **, t_graphinf *);
int			get_definitions(char **, t_graphinf *, t_room **, int);
int			get_links(char **, t_graphinf *, t_room *, int);
int			my_get_focused_nbr(char *, int, int);
int			check_break_point(char *, char *, t_room **, int *);
int			parse_room(t_room **, char *, int);

/*
** Graph functions
*/

t_room			*create_graph();
int			link_rooms(t_room *, char *, char *);
int			add_room(t_room **, char *, t_room_data *);

#endif /* !LEMIN_H_ */
