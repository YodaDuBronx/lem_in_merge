/*
** wordtab.h for lemin in /home/ballot_g/rendu/lem_in_merge/parser
**
** Made by Gregoire Ballot
** Login   <ballot_g@epitech.net>
**
** Started on  Mon Apr 21 15:42:01 2014 Gregoire Ballot
** Last update Wed Apr 30 15:33:17 2014 Gregoire Ballot
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

char	**my_str_to_wordtab(char *);
void	my_free_wordtab(char **);
int	my_wordtab_len(char **);

#endif /* !WORDTAB_H_ */
